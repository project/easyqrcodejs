(function ($, Drupal, once) {
  Drupal.behaviors.easyQRCodeJsAttach = {
    attach(context, settings) {
      
      let easyQRCodeClass = drupalSettings.easyqrcodejs.class || 'easyqrcodejs';
      let elements = once('easyqrcode', '.' + easyQRCodeClass, context);
      elements.forEach(function (i) {
        let element = $(i);
        let options = {
          drawer: 'canvas',
          width: element.data("width"),
          height: element.data("height"),
          colorLight: element.data("background"),
          colorDark: element.data("foreground"),
          logo: element.data("logo"),
          text: element.data("value"),
          onRenderingEnd: (qrCodeOptions, dataURL) => {
            if (element.data("download")) {
              element.on('click', () => {
                if (confirm(Drupal.t("Do you want to download the QR code?"))) {
                  var link = document.createElement('a');
                  // Add the name of the file to the link
                  link.download = element.data("download");
                  // Attach the data to the link
                  link.href = dataURL;
                  // Get the code to click the download link
                  link.click();
                  document.body.removeChild(link);
                }
              });
            }
          }
        };
        // Create QRCode Object
        let qrcode = new QRCode(i, options);
      });
    },
  };

}(jQuery, Drupal, once));
