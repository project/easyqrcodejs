<?php

namespace Drupal\easyqrcodejs\Element;

use Drupal\Core\Render\Element\HtmlTag;

/**
 * A render element that generates a QR code.
 *
 * QR code are generated using easyqrcodejs.js.
 *
 * @see https://github.com/ushelp/EasyQRCodeJS/
 *
 * For simplicity, not all easyqrcodejs options are exposed to
 * the render element.
 *
 * QR code specific properties:
 * - #width: QR code width, Unit: px,default 256 px
 * - #height: QR code height, Unit: px,default 256 px
 * - #background: The background color of QR code
 * - #foreground: The foreground color of QR code
 * - #logo: The logo to display in the middle of the qr-code.
 *    Examples:
 *      // Relative address, relative to webroot.
 *      #logo: "/demo/logo.png",
 *      // Absolute address.
 *      #logo: "http://127.0.0.1:8020/easy-qrcodejs/demo/logo.png",
 * - #value: The content encoded by QR code
 *
 * Usage example:
 * @code
 * $element['qrcode'] = [
 *   '#type' => 'easyqrcodejs',
 *   '#value' => 'https://www.drupal.org',
 * ];
 * @endcode
 *
 * @RenderElement("easyqrcodejs")
 */
class EasyQRCodeJs extends HtmlTag {

  /**
   * HTML tags allowed to render a QR code.
   *
   * @var array
   */
  protected static $validElements = [
    'div',
  ];

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;
    return [
      // QR code width in pixels.
      '#width' => 256,
      // QR code height in pixels.
      '#height' => 256,
      // QR code background color.
      '#background' => "#ffffff",
      // QR code foreground color.
      '#foreground' => "#000000",
      // A logo to render.
      '#logo' => '',
      // The content to encode as a QR code.
      '#value' => '',
      // HTML tag to render in.
      '#tag' => 'div',
      // Drupal render stuff.
      '#pre_render' => [
        [$class, 'preRenderHtmlTag'],
      ],
      // Additional HTML tag attributes.
      '#attributes' => [],
      // A filename to downlaod the QR-code when clicked.
      '#download' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function preRenderHtmlTag($element) {

    $easyqrcodejsClass = 'easyqrcodejs_qrcode';

    $element['#tag'] = 'div';

    $element['#attached']['drupalSettings']['easyqrcodejs']['class'] = $easyqrcodejsClass;

    $element['#attached']['library'][] = 'easyqrcodejs/render';
    $element['#attributes']['class'][] = $easyqrcodejsClass;

    $element['#attributes']['data-width'] = (string) $element['#width'];
    $element['#attributes']['data-height'] = (string) $element['#height'];
    $element['#attributes']['data-background'] = (string) $element['#background'];
    $element['#attributes']['data-foreground'] = (string) $element['#foreground'];
    $element['#attributes']['data-value'] = (string) $element['#value'];
    $element['#value'] = NULL;

    if (!empty($element['#logo'])) {
      $element['#attributes']['data-logo'] = (string) $element['#logo'];
    }

    if (!empty($element['#download'])) {
      $element['#attributes']['data-download'] = (string) $element['#download'];
    }

    return parent::preRenderHtmlTag($element);
  }

}
